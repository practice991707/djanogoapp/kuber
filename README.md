# Kubernetes манифесты для развертывания тестового Django приложения (#230501 из задачника).

# Вариант Hard 

* Исходное приложение Django Girls Blog (https://gitlab.com/practice991707/djanogoapp/docker-cicd-django) доработано под использование env переменных. K8s манифесты и Dockerfile расположены в директории manifests
```
kubectl create ns $Test
kubectl apply -f ./manifests/*.yml
```
